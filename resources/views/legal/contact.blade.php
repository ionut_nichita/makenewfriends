@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-1"></div>
    <div class="col-sm-10" style="color:black;">
        <!-- https://www.privacypolicygenerator.info/live.php?token=TgZ46AVi4aiLXvIFgpa411iCT5eQGxYn -->
        <h1>Contact</h1>

        <p>You can contact the staff regarding any issue at contact@wannabemyfriend.com</p>

        <h1>Removal of username</h1>
        <p>You can request for a total removal of your user name and related info from our site by sending a request at contact@wannabemyfriend.com .
            Each request will be processed manually in order to prevent malicious use of service.</p>
    </div>
    <div class="col-sm-1"></div>
</div>


@endsection