@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-1"></div>
    <div class="col-sm-10" style="font-size: large; font-weight: bolder;">
        Add at least one user name and meet lots of girls and boys! <br> No registration necessary. <br> <br>
    </div>
    <div class="col-sm-1"></div>
</div>

<div class="row">
    <div class="col-sm-1"></div>
    <div class="col-sm-10">
        <div class="panel panel-default">

            <div class="panel-heading clearfix">

                <span class="pull-left">
                    <h4 class="mt-5 mb-5">Create A New Profile To Be Able To Contact Other Users</h4>
                </span>

                <!--             <div class="btn-group btn-group-sm pull-right" role="group">
                <a href="{{ route('snap_users.snap_user.index') }}" class="btn btn-primary" title="Show All Snap User">
                    <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                </a>
            </div> -->

            </div>

            <div class="panel-body">

                @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
                @endif

                <form method="POST" action="{{ route('snap_users.snap_user.store') }}" accept-charset="UTF-8" id="create_snap_user_form" name="create_snap_user_form" class="form-horizontal" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    @include ('snap_users.form', [
                    'snapUser' => null,
                    ])

                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-10">
                            <input class="btn btn-primary" type="submit" value="Submit your profile">
                        </div>
                    </div>

                </form>

            </div>
        </div>

    </div>
    <div class="col-sm-1"></div>
</div>


<!-- start list insertion -->
<div class="panel-body panel-body-with-table">
    <div>
        Latest added user names. Just add your profile to see other's full info and picture. <br><br>
    </div>
    <div class="table-responsive">

        <table class="table table-striped ">
            <thead>
                <tr>
                    <th>Picture</th>
                    <th>Snapchat Username</th>
                    <th>Gender</th>
                    <th>Age</th>
                    <th>Description</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach($snapUsers as $snapUser)
                <tr>
                    <!-- <td><img src="pics/icons/femaledefaultuser.png" alt="" style="max-width: 50px;"></td> -->
                    <td><img style="max-width:100px;max: height 100px;" src="{{ $snapUser->userpic?$snapUser->userpic:($snapUser->gender=='female'?'/pics/icons/femaledefaultuser.png':'/pics/icons/maledefaultuser.png') }}"></td>
                    <td>{{ $snapUser->snap_username[0]."******" }}</td>
                    <td>{{ $snapUser->gender }}</td>
                    <td>{{ $snapUser->age }}</td>
                    <td>{{ $snapUser->description }}</td>

                    <td>

                        <form method="POST" action="{!! route('snap_users.snap_user.destroy', $snapUser->id) !!}" accept-charset="UTF-8">
                            <input name="_method" value="DELETE" type="hidden">
                            {{ csrf_field() }}

                            <div class="btn-group btn-group-xs pull-right" role="group">
                                <!-- <a href="{{ route('snap_users.snap_user.show', $snapUser->id ) }}" class="btn btn-info" title="Show User">
                                    <span class="glyphicon glyphicon-open" aria-hidden="true"></span>
                                </a> -->
                                <a href="mailto:contact@wannabemyfriend.com" class="btn btn-info" title="Report User">
                                    <span class="glyphicon  glyphicon-thumbs-down" aria-hidden="true"></span>
                                </a>
                                <!--  <a href="{{ route('snap_users.snap_user.edit', $snapUser->id ) }}" class="btn btn-primary" title="Edit Snap User">
                                            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                        </a> -->

                                <!-- <button type="submit" class="btn btn-danger" title="Delete Snap User" onclick="return confirm(&quot;Click Ok to delete Snap User.&quot;)">
                                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                        </button> -->
                            </div>

                        </form>

                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

    </div>
</div>

<br><br><br><br><br>
<!-- end list insertion -->




@endsection
