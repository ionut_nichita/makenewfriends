@extends('layouts.app')

@section('content')

@if(Session::has('success_message'))
<div class="alert alert-success">
    <span class="glyphicon glyphicon-ok"></span>
    {!! session('success_message') !!}

    <button type="button" class="close" data-dismiss="alert" aria-label="close">
        <span aria-hidden="true">&times;</span>
    </button>

</div>
@endif

<div class="panel panel-default">

    <div class="panel-heading clearfix">

        <div class="pull-left">
            <h4 class="mt-5 mb-5">Snap Users</h4>
        </div>

        <!--         <div class="btn-group btn-group-sm pull-right" role="group">
            <a href="{{ route('snap_users.snap_user.create') }}" class="btn btn-success" title="Create New Snap User">
                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
            </a>
        </div> -->

    </div>

    @if(count($snapUsers) == 0)
    <div class="panel-body text-center">
        <h4>No Snap Users Available.</h4>
    </div>
    @else
    <div class="panel-body panel-body-with-table">
        <div class="table-responsive">

            <table class="table table-striped ">
                <thead>
                    <tr>
                        <th>Picture</th>
                        <th>Snapchat Username</th>
                        <th>Gender</th>
                        <th>Age</th>
                        <th>Description</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($snapUsers as $snapUser)
                    <tr>
                        <!-- <td><img src="pics/icons/femaledefaultuser.png" alt="" style="max-width: 50px;"></td> -->
                        <td><a href="{{ route('snap_users.snap_user.show', $snapUser->id ) }}"><img src="{{ $snapUser->userpic?$snapUser->userpic:($snapUser->gender=='female'?'/pics/icons/femaledefaultuser.png':'/pics/icons/maledefaultuser.png') }}" alt="" style="max-width: 50px;"></a></td>
                        <td><a href="{{ route('snap_users.snap_user.show', $snapUser->id ) }}">{{ $snapUser->snap_username }}</a></td>
                        <td>{{ $snapUser->gender }}</td>
                        <td>{{ $snapUser->age }}</td>
                        <td>{{ $snapUser->description }}</td>

                        <td>

                            <form method="POST" action="{!! route('snap_users.snap_user.destroy', $snapUser->id) !!}" accept-charset="UTF-8">
                                <input name="_method" value="DELETE" type="hidden">
                                {{ csrf_field() }}

                                <div class="btn-group btn-group-xs pull-right" role="group">
                                    <a href="{{ route('snap_users.snap_user.show', $snapUser->id ) }}" class="btn btn-info" title="Show User">
                                        <span class="glyphicon glyphicon-open" aria-hidden="true"></span>
                                    </a>
                                    <a href="mailto:contact@wannabemyfriend.com" class="btn btn-info" title="Report User">
                                        <span class="glyphicon  glyphicon-thumbs-down" aria-hidden="true"></span>
                                    </a>
                                    <!--  <a href="{{ route('snap_users.snap_user.edit', $snapUser->id ) }}" class="btn btn-primary" title="Edit Snap User">
                                            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                        </a> -->

                                    <!-- <button type="submit" class="btn btn-danger" title="Delete Snap User" onclick="return confirm(&quot;Click Ok to delete Snap User.&quot;)">
                                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                        </button> -->
                                </div>

                            </form>

                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
    </div>

    <div>
    </div>

    <div class="panel-footer">
        {!! $snapUsers->render() !!}
    </div>

    @endif

</div>
@endsection
