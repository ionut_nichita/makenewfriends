<div class="form-group {{ $errors->has('snap_username') ? 'has-error' : '' }}">
    <label for="snap_username" class="col-md-2 control-label">Snapchat Username</label>
    <div class="col-md-10">
        <input required class="form-control" name="snap_username" type="text" id="snap_username" value="{{ old('snap_username', optional($snapUser)->snap_username) }}" minlength="1" placeholder="Enter snap username here...">
        {!! $errors->first('snap_username', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('gender') ? 'has-error' : '' }}">
    <label for="gender" class="col-md-2 control-label">Gender</label>
    <div class="col-md-10">
        <select required class="form-control" name="gender" type="text" id="gender" value="{{ old('gender', optional($snapUser)->gender) }}" minlength="1" placeholder="Enter gender here...">
            <option value="female">Female</option>
            <option value="male">Male</option>
            <option value="other">Other</option>
        </select>
        {!! $errors->first('gender', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('age') ? 'has-error' : '' }}">
    <label for="age" class="col-md-2 control-label">Age (optional)</label>
    <div class="col-md-10">
        <input class="form-control" name="age" type="number" id="age" min="13" max="120" value="{{ old('age', optional($snapUser)->age) }}" placeholder="Enter age here...">
        {!! $errors->first('age', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
    <label for="description" class="col-md-2 control-label">Description (optional)</label>
    <div class="col-md-10">
        <textarea maxlength="350" class="form-control" name="description" type="text" id="description" value="Describe yourself ..." placeholder="Enter description here..."></textarea>
        {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('userpic') ? 'has-error' : '' }}">
    <label for="userpic" class="col-md-2 control-label">Upload picture (optional)</label>
    <div class="col-md-10">
        <input class="form-control" name="userpic" type="file" id="userpic" value="Upload your picture (optional) " placeholder="Upload picture here..." accept="image/*">
        <!-- <input type="file" id="userpic" name="userpic" accept="image/*"> -->

        {!! $errors->first('userpic', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@if (\App::environment('production'))
<div class="form-group">
    <label for="cpt" class="col-md-2 control-label">Captcha</label>
    <div class="col-md-10">
        {!! app('captcha')->display() !!}

        @if ($errors->has('g-recaptcha-response'))

        <span class="help-block">

            <strong>{{ $errors->first('g-recaptcha-response') }}</strong>

        </span>

        @endif
    </div>
</div>
@endif
</div>