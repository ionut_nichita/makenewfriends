@extends('layouts.app')

@section('content')
<img src="/{{ $snapUser->userpic?$snapUser->userpic:($snapUser->gender=='female'?'pics/icons/femaledefaultuser.png':'pics/icons/maledefaultuser.png') }}" alt="" style="max-height:400px;">
<div class="panel panel-default">
    <div class="panel-heading clearfix">
        <span class="pull-left">
            <h4 class="mt-5 mb-5">{{ isset($title) ? $title : 'Snap User' }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('snap_users.snap_user.destroy', $snapUser->id) !!}" accept-charset="UTF-8">
                <input name="_method" value="DELETE" type="hidden">
                {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('snap_users.snap_user.index') }}" class="btn btn-primary" title="Show All Snap User">
                        <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                    </a>

                    <!--                     <a href="{{ route('snap_users.snap_user.create') }}" class="btn btn-success" title="Create New Snap User">
                        <span class="glyphicon glyphsicon-plus" aria-hidden="true"></span>
                    </a>
                    
                    <a href="{{ route('snap_users.snap_user.edit', $snapUser->id ) }}" class="btn btn-primary" title="Edit Snap User">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    </a> -->

                    <!--                     <button type="submit" class="btn btn-danger" title="Delete Snap User" onclick="return confirm(&quot;Click Ok to delete Snap User.?&quot;)">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    </button> -->
                </div>
            </form>

        </div>

    </div>


    <div class="panel-body">

        <dl class="dl-horizontal">

            <dt>Snapchat Username</dt>
            <dd>{{ $snapUser->snap_username }}</dd>
            <dt>Gender</dt>
            <dd>{{ $snapUser->gender }}</dd>
            <dt>Age</dt>
            <dd>{{ $snapUser->age }}</dd>

        </dl>

    </div>
</div>

@endsection