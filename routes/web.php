<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'throttle:30'], function () {
     Route::get('/', 'SnapUsersController@create')->name('mainPage');

     Route::get('/privacy-policy', function () {
          return view('legal.privacy');
     });
     Route::get('/contact', function () {
          return view('legal.contact');
     });
});

Route::group([
     'prefix' => 'snap_users',
     'middleware' => 'throttle:30'
], function () {
     Route::get('/', 'SnapUsersController@index')
          ->name('snap_users.snap_user.index');
     Route::get('/create', 'SnapUsersController@create')
          ->name('snap_users.snap_user.create');
     Route::get('/show/{snapUser}', 'SnapUsersController@show')
          ->name('snap_users.snap_user.show');
     Route::get('/{snapUser}/edit', 'SnapUsersController@edit')
          ->name('snap_users.snap_user.edit');
     Route::post('/', 'SnapUsersController@store')
          ->name('snap_users.snap_user.store');
     Route::put('snap_user/{snapUser}', 'SnapUsersController@update')
          ->name('snap_users.snap_user.update');
     Route::delete('/snap_user/{snapUser}', 'SnapUsersController@destroy')
          ->name('snap_users.snap_user.destroy');

     // Route::get('site-register', 'SiteAuthController@siteRegister');
     // Route::post('site-register', 'SiteAuthController@siteRegisterPost');

});



Route::resource('snapUsers', 'SnapUserController');