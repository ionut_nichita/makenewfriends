<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\SnapUser;
use Illuminate\Http\Request;
use Exception;
use App\Service;
use App\Service\CommonService;
use Illuminate\Validation\Rule;

class SnapUsersController extends Controller
{

    /**
     * Display a listing of the snap users.
     *
     */
    public function index(Request $request)
    {
        $sessionCheck = $request->session()->get('isProfileRegistered');
        if (!$sessionCheck) {
            return redirect()->route('snap_users.snap_user.create');
        }

        $snapUsers = SnapUser::orderBy('id', 'desc')->paginate(25);

        return view('snap_users.index', compact('snapUsers'));
    }

    /**
     * Show the form for creating a new snap user.
     *
     * @return Illuminate\View\View
     */
    public function create(Request $request)
    {
        //redirect to list if registered
        $sessionCheck = $request->session()->get('isProfileRegistered');
        if ($sessionCheck) {
            return redirect()->route('snap_users.snap_user.index');
        }

        //----

        $snapUsers = SnapUser::orderBy('id', 'desc')->paginate(25);

        return view('snap_users.create', compact('snapUsers'));
    }

    /**
     * Store a new snap user in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {

            $ip = $request->ip();
            $data = $this->getData($request);

            //check start
            //TODO: check this live!

            if (!\App::environment('local')) {
                $commonSrv = new CommonService();
                $postCondition =  /* $commonSrv->checkIpCountOk($ip) &&  */ !$commonSrv->isUsernameRegisteredLast24h($request->get('snap_username'));

                /*  echo $request->get('snap_username');
                var_dump($postCondition);
                die('a'); */
                if (!$postCondition) {
                    throw new Exception('You cannot post the same username in less than 24 hours!');
                }
            }
            //check end

            $data['ip'] =  $ip;

            $file = $request->file('userpic');

            if ($request->hasFile('userpic')) {
                $path = $file->store('userpics/media', ['disk' => 'public']);
                $data['userpic'] = 'storage/' . $path;
            }

            SnapUser::create($data);

            session(['isProfileRegistered' => true]);

            return redirect()->route('snap_users.snap_user.index')
                ->with('success_message', 'User was successfully added.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => $exception->getMessage()]);
        }
    }

    /**
     * Display the specified snap user.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    public function show($id)
    {
        $snapUser = SnapUser::findOrFail($id);

        return view('snap_users.show', compact('snapUser'));
    }

    /**
     * Show the form for editing the specified snap user.
     *
     * @param int $id
     *
     * @return Illuminate\View\View
     */
    /*   public function edit($id)
    {
        $snapUser = SnapUser::findOrFail($id);


        return view('snap_users.edit', compact('snapUser'));
    } */

    /**
     * Update the specified snap user in the storage.
     *
     * @param int $id
     * @param Illuminate\Http\Request $request
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    /*     public function update($id, Request $request)
    {
        try {

            $data = $this->getData($request);

            $snapUser = SnapUser::findOrFail($id);
            $snapUser->update($data);

            return redirect()->route('snap_users.snap_user.index')
                ->with('success_message', 'User was successfully updated.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    } */

    /**
     * Remove the specified snap user from the storage.
     *
     * @param int $id
     *
     * @return Illuminate\Http\RedirectResponse | Illuminate\Routing\Redirector
     */
    /*     public function destroy($id)
    {
        try {
            $snapUser = SnapUser::findOrFail($id);
            $snapUser->delete();

            return redirect()->route('snap_users.snap_user.index')
                ->with('success_message', 'User was successfully deleted.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    } */


    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
            'snap_username' => [
                "regex:/^(?!.*\.\.|.*\_\_|.*\-\-)(?!.*\.$|.*\_$|.*\-$)(?!.*\.\-|.*\-\.|.*\-\_|.*\_\-|.*\.\_|.*\_\.)[a-zA-Z]+[\w.-][0-9A-z]{0,15}$/i"
            ],
            'gender' => [
                "string",
                Rule::in(['male', 'female', 'other'])
            ],
            'age' => 'numeric|min:1|max:150|nullable',
            'description' => 'string|max:350|nullable',
            // 'g-recaptcha-response' => 'required|captcha',
        ];
        if (\App::environment('production')) {
            $rules['g-recaptcha-response'] =  'required|captcha';
        }

        $data = $request->validate($rules);

        return $data;
    }
}
