<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSnapUserRequest;
use App\Http\Requests\UpdateSnapUserRequest;
use App\Repositories\SnapUserRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class SnapUserController extends AppBaseController
{
    /** @var  SnapUserRepository */
    private $snapUserRepository;

    public function __construct(SnapUserRepository $snapUserRepo)
    {
        $this->snapUserRepository = $snapUserRepo;
    }

    /**
     * Display a listing of the SnapUser.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $snapUsers = $this->snapUserRepository->all();

        return view('snap_users.index')
            ->with('snapUsers', $snapUsers);
    }

    /**
     * Show the form for creating a new SnapUser.
     *
     * @return Response
     */
    public function create()
    {
        return view('snap_users.create');
    }

    /**
     * Store a newly created SnapUser in storage.
     *
     * @param CreateSnapUserRequest $request
     *
     * @return Response
     */
    public function store(CreateSnapUserRequest $request)
    {
        $input = $request->all();

        $snapUser = $this->snapUserRepository->create($input);

        Flash::success('Snap User saved successfully.');

        return redirect(route('snapUsers.index'));
    }

    /**
     * Display the specified SnapUser.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $snapUser = $this->snapUserRepository->find($id);

        if (empty($snapUser)) {
            Flash::error('Snap User not found');

            return redirect(route('snapUsers.index'));
        }

        return view('snap_users.show')->with('snapUser', $snapUser);
    }

    /**
     * Show the form for editing the specified SnapUser.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $snapUser = $this->snapUserRepository->find($id);

        if (empty($snapUser)) {
            Flash::error('Snap User not found');

            return redirect(route('snapUsers.index'));
        }

        return view('snap_users.edit')->with('snapUser', $snapUser);
    }

    /**
     * Update the specified SnapUser in storage.
     *
     * @param int $id
     * @param UpdateSnapUserRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSnapUserRequest $request)
    {
        $snapUser = $this->snapUserRepository->find($id);

        if (empty($snapUser)) {
            Flash::error('Snap User not found');

            return redirect(route('snapUsers.index'));
        }

        $snapUser = $this->snapUserRepository->update($request->all(), $id);

        Flash::success('Snap User updated successfully.');

        return redirect(route('snapUsers.index'));
    }

    /**
     * Remove the specified SnapUser from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $snapUser = $this->snapUserRepository->find($id);

        if (empty($snapUser)) {
            Flash::error('Snap User not found');

            return redirect(route('snapUsers.index'));
        }

        $this->snapUserRepository->delete($id);

        Flash::success('Snap User deleted successfully.');

        return redirect(route('snapUsers.index'));
    }
}
