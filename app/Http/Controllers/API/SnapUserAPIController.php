<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSnapUserAPIRequest;
use App\Http\Requests\API\UpdateSnapUserAPIRequest;
use App\Models\SnapUser;
use App\Repositories\SnapUserRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\SnapUserResource;
use Response;

/**
 * Class SnapUserController
 * @package App\Http\Controllers\API
 */

class SnapUserAPIController extends AppBaseController
{
    /** @var  SnapUserRepository */
    private $snapUserRepository;

    public function __construct(SnapUserRepository $snapUserRepo)
    {
        $this->snapUserRepository = $snapUserRepo;
    }

    /**
     * Display a listing of the SnapUser.
     * GET|HEAD /snapUsers
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $snapUsers = $this->snapUserRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(SnapUserResource::collection($snapUsers), 'Snap Users retrieved successfully');
    }

    /**
     * Store a newly created SnapUser in storage.
     * POST /snapUsers
     *
     * @param CreateSnapUserAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateSnapUserAPIRequest $request)
    {
        $input = $request->all();

        $snapUser = $this->snapUserRepository->create($input);

        return $this->sendResponse(new SnapUserResource($snapUser), 'Snap User saved successfully');
    }

    /**
     * Display the specified SnapUser.
     * GET|HEAD /snapUsers/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var SnapUser $snapUser */
        $snapUser = $this->snapUserRepository->find($id);

        if (empty($snapUser)) {
            return $this->sendError('Snap User not found');
        }

        return $this->sendResponse(new SnapUserResource($snapUser), 'Snap User retrieved successfully');
    }

    /**
     * Update the specified SnapUser in storage.
     * PUT/PATCH /snapUsers/{id}
     *
     * @param int $id
     * @param UpdateSnapUserAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSnapUserAPIRequest $request)
    {
        $input = $request->all();

        /** @var SnapUser $snapUser */
        $snapUser = $this->snapUserRepository->find($id);

        if (empty($snapUser)) {
            return $this->sendError('Snap User not found');
        }

        $snapUser = $this->snapUserRepository->update($input, $id);

        return $this->sendResponse(new SnapUserResource($snapUser), 'SnapUser updated successfully');
    }

    /**
     * Remove the specified SnapUser from storage.
     * DELETE /snapUsers/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var SnapUser $snapUser */
        $snapUser = $this->snapUserRepository->find($id);

        if (empty($snapUser)) {
            return $this->sendError('Snap User not found');
        }

        $snapUser->delete();

        return $this->sendSuccess('Snap User deleted successfully');
    }
}
