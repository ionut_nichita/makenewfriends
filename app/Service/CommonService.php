<?php

namespace App\Service;

use App\Models\SnapUser;
use Carbon\Carbon;


class CommonService
{
    const ALLOWED_USERS_PER_IP = 5;

    public function checkIpCountOk(string $ip): bool
    {
        $count = SnapUser::where('ip', '=', $ip)->count();
        // print_r(empty($count));
        if (empty($count) || $count <= self::ALLOWED_USERS_PER_IP) {
            return true;
        }
        // echo "falllse";
        return false;
    }

    public function isUsernameRegisteredLast24h(string $snapUsername): bool
    {
        $usr = SnapUser::where([['created_at', '>', Carbon::now()->subDays(1)->toDateTimeString()], ['snap_username','=',$snapUsername]])->first();
        // var_dump($usr);
        // die();
        if ($usr) {
            return true;
        }

        return false;
    }
}
