<?php

namespace App\Repositories;

use App\Models\SnapUser;
use App\Repositories\BaseRepository;

/**
 * Class SnapUserRepository
 * @package App\Repositories
 * @version May 13, 2021, 8:30 pm UTC
*/

class SnapUserRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'snap_username',
        'gender',
        'age',
        'userpic',
        'description',
        'ip'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SnapUser::class;
    }
}
