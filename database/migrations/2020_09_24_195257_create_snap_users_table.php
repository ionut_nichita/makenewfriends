<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSnapUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('snap_users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('snap_username');
            $table->enum('gender', ['female', 'male', 'other']);
            $table->string('age')->nullable();
            $table->string('userpic')->nullable();
            $table->text('description')->nullable();
            $table->string('ip')->nullable();
            // $table->string('social_network')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('snap_users');
    }
}
